# 'Lemonade Stand' Programming Challenge (Backend)


## Introduction

After many years of successful operation, the Lemonade Stand has decided it's time to go hightech! They would like to implement a sales tracking and reporting system to help manage the
incredible amounts of lemonade they sell on a daily basis, and automatically calculate
appropriate commissions for their hard-working sales staff. And you have been picked to do the
job!!!


## General Guidelines

- The specifications for this challenge have been deliberately left somewhat open, to allow
you to come up with your own solution to the requirements. Please feel free to solve the
problem as you see fit to best serve the client.

- This task has been designed to take about 2-3 hours to complete. 
If this is your first experience with Django then it will probably take more time...

- Please take into account that code & product quality is more important than time taken.

- We are much more interested in your ability to write functional Python & Django code
than the ability to design visually appealing HTML.


## Requirements

The following requirements have been provided by the Lemonade Stand management:

- We need to be able to track all our sales staff. For each we would like to store their
name, position (Junior, Senior or Manager) and commission percentage (they receive
this percentage of the price of each sale they make as a sales commission). Our current
sales team is composed of:
    - Jeff Terry, Senior, 10% commission.
    - Thomas Black, Manager, 20% commission.
    - John Rice, Junior, 5% commission.
    - Larry Long, Junior, 0% commission.

- We sell four different types of lemonade at our stand, and each is priced differently
based on its quality and uniqueness. Here is our current price list:
    - Fresh Lemon Lemonade @ $1.50/cup.
    - Orange & Lemon Splash @ $2.00/cup.
    - Sugary Shocker @ $3.00/cup.
    - Wild Whiskey Whack @ $5.50/cup.

- We would like to keep track of every sale made at our stand, so that we will be able to
generate reports later on which items (and how many) were sold on what dates, and which salesperson made each sale. We will also want to calculate commissions for each
salesperson between certain dates.

- We would like the system to be able flexible enough to handle future changes in sales
personnel, items and prices.


## Database Design

As a first step, please create a database diagram (or ERD) showing the various database tables
required to implement your proposed solution. Please include the fields of each table and
indicate the relationships between tables.


## Model Implementation

Please create a new Django application for the sales system. You may setup the URLs for this
application in any way you see fit. In this application, please create the appropriate models for
the database design you completed previously. You may also add these models to the admin
system and populate any initial data you feel is necessary.
 

## Sales Entry Form

At this stage, we need to create an online form that will allow the cashier to enter the details of
each sale made. Please create a new web page at URL `/sales/form/` which will provide input
fields for the sales data tracked by the system. (Please refer to the Requirements section above).
Once submitted and validated, the sale should be written to the database and a blank form
presented for entering a new sale.

## Minor Graphics Update
The graphics designer has requested we change the background color of the pages from white to
a pastel blue (#ebf7fc). Could you please implement this change for the homepage and any new
pages you create in the next sections.
 

## Sales & Commission Report

We now need a report that presents the sales made and commissions earned by a salesperson
between specific dates. Please create a new web page at URL `/sales/report/` which will present
this report. It should contain a short form allowing the user the enter the start date and end date
of the report's date range, and also choose which salesperson the report is to be generated for.
Once submitted, the report should appear as indicated below, sorted by the date of the sale. The
report should calculate the commission earned by the salesperson for each sale.

| Date                  | Items Sold       | Total Price | Commission Earned |
|-----------------------|------------------|-------------|-------------------|
| May 12, 2020 12:45pm  | `list of items`  | $12.90      | $1.29             |
| May 12, 2020 02:14pm  | `list of items`  | $5.50       | $0.55             |
| **TOTALS**            |                  | $18.40      | $1.84             |


## Aesthetic Improvements

A few small aesthetic improvements and we're done! Could you please add the following
functionality to the Sales Entry Form:

- For any form fields that fail validation, have them change color to a light red to clearly
indicate which fields contain invalid values.

- Once the form has been successfully submitted and the data saved, please add a clear
message on top of the now empty form that the data has been saved. This message
should disappear after 5 seconds.

- Please add a read-only text field at the bottom of the form labeled "Total Price”. This
field should dynamically update with the total price of the sale as the cashier chooses the
items that make up the sale.

